#!/usr/bin/python
import sys,sqlite3
from prettytable import PrettyTable
from argparse import ArgumentParser,RawDescriptionHelpFormatter

"""
1) read hash file
2) for each hash, see if user/hash already present
3) if not present, add
4) read password out file
5) update table where hash is found
6)

"""



def dbc(db):
        conn = sqlite3.connect(db)
        c = conn.cursor()
        return c

# Check if table exists, create if not
def create_table(cursor,table):
        try:
                cursor.execute('CREATE TABLE "{0}" (Id INTEGER PRIMARY KEY,\
                           username varchar(60), password varchar(60),\
                        function varchar(60),location varchar(60),\
                         notes varchar(200), hash varchar(120))'.format(table),)
                cursor.connection.commit()
        except Exception, err:
                pass



# Check if in table
def hash_present(cursor,u,h):
        cursor.execute("select hash from my_passwords where username=? AND hash=?",(u,h))
        if cursor.fetchone():
                print "Already present: {0}:{1}".format(u,h)
                return True
        else: return False

# Add to table
def insert_userhashes(cursor,row):
        # row = (username,hash,plaintext)

        try:
                cursor.execute('INSERT INTO "my_passwords" VALUES(Null,?,?,?,?,?,?)',row)
                cursor.connection.commit()
        except Exception,err:
                if 'no such table' in str(err):
                        print "no table exists"
                        # create table
                else:
                        print 'exception'
                        print str(err)


def update_password(cursor,hashvalue,password):
        try:
                cursor.execute("update my_passwords set password=? where hash=?",(password,hashvalue))
                cursor.connection.commit()
                if cursor.rowcount == 0:
                        print "No record found for: ",hashvalue,password
                else:
                        print "{0} was updated".format(hashvalue)
        except Exception as e:
                print str(e)

def table_exists(cursor):
        cursor.execute("select name from sqlite_master where type='table' and name='my_passwords'")
        if cursor.fetchone():
                return True
        print "No Table"
        return False



def insert_hash_from_file(hashfile,function,location,notes,windows=False):
        f = open(hashfile,'r')
        lines = f.read().splitlines()
        for line in lines:
                try:
                    if windows:
                        u,h = line.split(":")[0],line.split(":")[2]
                    else:
                        u,h = line.split(':')[:2]
                    if not hash_present(cursor,u,h):
                            insert_userhashes(cursor,(u,'',function,location,notes,h))
                            print "Added {0}:{1}".format(u,h)
                    else:
                            pass
                except Exception as e:
                        print str(e)

def insert_plain_pass(user,password,function,location,notes):
    insert_userhashes(cursor,(user,password,function,location,notes,""))

def add_passwords_from_file():
        f = open(sys.argv[2],'r')
        lines = f.read().splitlines()
        for line in lines:
                try:
                        h,p = line.split(":",1)[:2]
                        update_password(cursor,h,p)
                except ValueError as e:
                        print str(e)


def show_all(cursor):
        try:
            cols = ["username","password","function","location","notes","hash"]
            pt = PrettyTable(cols)
            pt.padding_width = 1
            cursor.execute("select * from my_passwords order by username")
            rows =  cursor.fetchall()
            for row in rows:
                    pt.add_row(row[1:])
            print (pt)
        except Exception, e:
            print str(e)
            cursor.execute("select * from my_passwords order by username")
            rows =  cursor.fetchall()
            print "Username,Password,Function,Location,Notes,Hash"
            for row in rows:
                    print ",".join(map(str,row[1:]))
                    #print "{0}{1}".format(row[1],row[3])


cursor = dbc('passwords.sqlite')

if table_exists(cursor):
        pass
else:
        print "Creating table"
        create_table(cursor,'my_passwords')
ArgumentParser
def main():
    parser = ArgumentParser(add_help=True, 
        formatter_class=RawDescriptionHelpFormatter,epilog = 'Usage examples:\n./hcm.py -ap admin:testpass123\n./hcm.py -w -ah testfiles/win.hash\n./hcm.py -ah testfiles/linux.hash\n./hcm.py -ah testfiles/linux.hash -f "system hashes" -l "192.168.2.1 /etc/shadow"\n./hcm.py -ar ./cracked/hashcat.pot')
    addhash = parser.add_argument_group('addhashes')
    parser.add_argument("-s", "--show", help="Show all credentials in db", action="store_true", required=False)
    parser.add_argument("-ac", "--addcracked", help='Add hashcat results from file', required=False)
    addhash.add_argument("-ah", "--addhash", help="Add hashfile captured from target", required=False) #, required=False)
    addhash.add_argument("-ap", "--addpass", help="Add plaintext password captured from target", required=False) #, required=False)
    addhash.add_argument("-f", "--function", help="Domain,Application,etc the password is used for", required=False)
    addhash.add_argument("-l", "--location", help="Add hashfile captured from target", required=False)
    addhash.add_argument("-n", "--notes", help="Additional notes regarding this file", required=False)
    addhash.add_argument("-w", "--winhash", help="Use this switch if the hash is a windows NTLM/LM hash", action="store_true")


    args = parser.parse_args()
    if args.show:
        show_all(cursor)
    if args.addcracked:
        add_passwords_from_file()
    if args.addhash:
        hashfile = args.addhash
        if not args.function:
            function = raw_input("What does the password access? (eg:system,domain,app): ")
        else: function = args.function
        if args.winhash:
            winhash = True
        else: winhash = False
        if not args.location:
            location = raw_input("Where the hash was found: ")
        else: location = args.location
        if not args.notes:
            notes = raw_input("Any additional notes? (200 char limit): ")
        else: notes = args.notes
        insert_hash_from_file(hashfile,function,location,notes,winhash)

    if args.addpass:
        username,password = args.addpass.split(":")
        if not args.function:
            function = raw_input("What does the password access? (eg:system,domain,app)> ")
        else: function = args.function
        if args.winhash:
            winhash = True
        else: winhash = False
        if not args.location:
            location = raw_input("Where the password creds were found (eg: 10.20.0.1/opt/backups)> ")
        else: location = args.location
        if not args.notes:
            notes = raw_input("Any additional notes? (200 char limit)> ")
        else: notes = args.notes
        insert_plain_pass(username,password,function,location,notes)

main()

